// ChatSection.jsx
import React from "react";
import "./RoomPage.css";

const ChatSection = () => {
  return (
    <div className="main-right d-flex flex-column text-center">
      <h5 className="mt-2">Chat ☕</h5>
      <div className="main-chat-window">
        <div className="messages-content">{/* Messages content here */}</div>
      </div>
      <div className="main-message-container d-flex justify-content-center p-3 align-items-center">
        <input
          id="input-message"
          type="text"
          autoComplete="off"
          placeholder="Write something.."
        />
        <div id="send" className="options-button chat">
          <button type="button">
            <img className="logo-btn" alt="video-logo" src="/send.png" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default ChatSection;
