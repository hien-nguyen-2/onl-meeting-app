import React, { useState } from "react";
//import { useHistory } from 'react-router-dom';
import "./RoomPage.css";
import ChatSection from "./ChatSection";

function RoomPage() {
  const [isChatVisible, setChatVisible] = useState(false);
  const [mainLeftFlex, setMainLeftFlex] = useState(1); // Initially, set it to full screen (flex: 1)

  const toggleChat = () => {
    setChatVisible(!isChatVisible);
    setMainLeftFlex(isChatVisible ? 1 : 0.8); // Toggle flex value between 1 (full screen) and 0.7 (7:3)
  };

  return (
    <div>
      <div className="header">
          <h4>
            <b>ON◡LINE MEETING</b>
          </h4>
      </div>
      <div className="main d-flex">
        <div className="main-left d-flex flex-column" style={{ flex: mainLeftFlex }}>
          <div className="screens d-flex justify-content-center p-4 align-items-center">
            <div id="video-grid"></div>
          </div>
          <div className="options d-flex p-3">
            <div className="options-left d-flex">
              <div id="stopVideo" className="options-button">
                <button type="button">
                  <img className="logo-btn" alt="video-logo" src="/cam.png" />
                </button>
              </div>
              <div id="muteBtn" className="options-button">
                <button type="button">
                  <img className="logo-btn" alt="mic-logo" src="/mic2.png" />
                </button>
              </div>
              <div id="showChat" className="options-button" onClick={toggleChat}>
                <button type="button">
                  <img className="logo-btn" alt="chat-logo" src="/chat2.png" />
                </button>
              </div>
            </div>
            <div className="options-right d-flex ms-auto">
              <div id="leaveBtn" className="leave-room">
                <button type="button">Leave Room</button>
              </div>
              <div id="zoomButton" className="options-button">
                <button type="button">
                  <img
                    className="logo-btn"
                    alt="video-logo"
                    src="/full-screen1.png"
                  />
                </button>
              </div>
            </div>
          </div>
        </div>
        {isChatVisible && <ChatSection />}

      </div>
      <div class="footer"></div>
    </div>
  );
}

export default RoomPage;
