import React, { useState } from "react";
//import { useHistory } from 'react-router-dom';
import "./AdminPage.css";

function AdminPage() {
  //const history = useHistory();

  // State to manage capacity selection and generated room URL
  const [capacity, setCapacity] = useState("50");
  const [roomUrl, setRoomUrl] = useState("");

  // Function to handle capacity selection
  const handleCapacityChange = (e) => {
    setCapacity(e.target.value);
  };

  // Function to generate a new room URL
  const generateRoomUrl = () => {
    // Generate a unique room URL based on capacity (e.g., using timestamp)
    const newRoomUrl = `room-${capacity}-${Date.now()}`;
    setRoomUrl(newRoomUrl);

    // Navigate to the new RoomPage with the generated URL
    //history.push(`/room/${newRoomUrl}`);
  };

  return (
    <div className="admin-page d-flex justify-content-center flex-column align-items-center">
      <div className="before-create">
        <h1>
          <b>
            ON◡LINE<br></br>MEETING
          </b>
        </h1>
        <div className="capa m-3">
          <label htmlFor="capacity">Room Capacity:</label>
          <select
            className="capa-wrap m-3"
            id="capacity"
            value={capacity}
            onChange={handleCapacityChange}
          >
            <option value="50">50 People</option>
            <option value="100">100 People</option>
          </select>
        </div>
        <button onClick={generateRoomUrl}>Create Meeting</button>
      </div>
      {roomUrl && (
        <div class="invite">
          <h5>Invite Friends🌷</h5>

          <div class="invite-fr row g-3 justify-content-center flex-row align-items-center mt-3">
            <div class="col-1 p-0">
              <h5>🔗</h5>
            </div>
            <div class="col-sm-8 room-link p-2 mt-0 ml-2">
              <a href={`/room/${roomUrl}`} target="_blank" rel="noopener noreferrer">
                {roomUrl}
              </a>
            </div>
            <div className="col-sm-2 mt-0">
              <button type="button">
                <img className="logo-btn" alt="logo-btn" src="/mail.png" />
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default AdminPage;
