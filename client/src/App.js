import React from 'react';
import { Route, Routes } from 'react-router-dom';
//import './App.css'; // You can uncomment this line if you have specific CSS for App.

// Import components for Admin and Room pages
import AdminPage from './components/AdminPage';
import RoomPage from './components/RoomPage';

function App() {
    return (
        <Routes>
          <Route path="/" element={<AdminPage />} />
          <Route path="/room/:code" element={<RoomPage />} />
        </Routes>
    );
  }

export default App;
